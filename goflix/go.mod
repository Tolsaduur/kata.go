module kata.go/goflix

go 1.13

require (
	github.com/auth0/go-jwt-middleware v1.0.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/form3tech-oss/jwt-go v3.2.2+incompatible // indirect
	github.com/gorilla/mux v1.8.0
	github.com/jmoiron/sqlx v1.3.1
	github.com/lib/pq v1.10.0
	github.com/stretchr/testify v1.7.0
)
