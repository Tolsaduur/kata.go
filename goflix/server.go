package main

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

type server struct {
	router *mux.Router
	store  Store
}

func newServer() *server {
	s := &server{
		router: mux.NewRouter(),
	}

	s.routes()
	return s
}

// Méthode permettant de déléguer le routing à gorilla/mux.
// On encapsule serveHTTP par le middleware logRequestMiddleware pour logger toutes les requêtes demandées.
func (s *server) serveHTTP(w http.ResponseWriter, r *http.Request) {
	logRequestMiddleware(s.router.ServeHTTP).ServeHTTP(w, r)
}

func (s *server) respond(w http.ResponseWriter, _ *http.Request, data interface{}, status int) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(status)

	if data == nil {
		return
	}

	err := json.NewEncoder(w).Encode(data)
	checkError(err)
}

func (s *server) decode(w http.ResponseWriter, r *http.Request, value interface{}) error {
	return json.NewDecoder(r.Body).Decode(value)
}
