package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	fmt.Println("GoFlix")

	if err := run(); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func run() error {
	srv := newServer()
	srv.store = &dbStore{}
	err := srv.store.Open()
	if err != nil {
		return err
	}

	defer srv.store.Close()

	// Permet de faire en sorte que les routes à partir de "/" soient gérer pour le router
	// que l'on a définit (en l'ocurrence gorilla/mux).
	http.HandleFunc("/", srv.serveHTTP)
	log.Printf("Serving HTTP on port 9000")
	err = http.ListenAndServe(":9000", nil)
	checkError(err)

	return nil
}

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
