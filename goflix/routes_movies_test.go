package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testStore struct {
	movieId int64
	movies  []*Movie
}

func (t testStore) Open() error {
	return nil
}

func (t testStore) Close() error {
	return nil
}

func (t testStore) GetMovies() ([]*Movie, error) {
	return t.movies, nil
}

func (t testStore) CreateMovie(m *Movie) error {
	t.movieId++
	m.ID = t.movieId
	t.movies = append(t.movies, m)

	return nil
}

func (t testStore) CheckUser(username string, password string) bool {
	return username == "Tarte" && password == "tatiN"
}

func (t testStore) GetMovieById(id int64) (*Movie, error) {
	for _, m := range t.movies {
		if m.ID == id {
			return m, nil
		}
	}
	return nil, nil
}

func TestMovieCreateUnit(t *testing.T) {
	// Create server with test DB
	srv := newServer()
	srv.store = &testStore{}

	// Prepare JSON Body
	p := struct {
		Title       string `json:"title"`
		ReleaseDate string `json:"release_date"`
		Duration    int    `json:"duration"`
		TrailerURL  string `json:"trailer_url"`
	}{
		Title:       "Monty Python : La vie de Brian",
		ReleaseDate: "1979-09-17",
		Duration:    92,
		TrailerURL:  "https://www.youtube.com/watch?v=f9fJpOdJtPg",
	}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(p)
	assert.Nil(t, err)

	r := httptest.NewRequest("POST", "/api/movies/", &buf)
	w := httptest.NewRecorder()

	srv.handleMovieCreate()(w, r)
	assert.Equal(t, http.StatusOK, w.Code)
}

func TestIntegrationMovieCreateUnit(t *testing.T) {
	// Create server with test DB
	srv := newServer()
	srv.store = &testStore{}

	// Prepare JSON Body
	p := struct {
		Title       string `json:"title"`
		ReleaseDate string `json:"release_date"`
		Duration    int    `json:"duration"`
		TrailerURL  string `json:"trailer_url"`
	}{
		Title:       "Monty Python : La vie de Brian",
		ReleaseDate: "1979-09-17",
		Duration:    92,
		TrailerURL:  "https://www.youtube.com/watch?v=f9fJpOdJtPg",
	}

	var buf bytes.Buffer
	err := json.NewEncoder(&buf).Encode(p)
	assert.Nil(t, err)

	r := httptest.NewRequest("POST", "/api/movies/", &buf)
	token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MTkzMzk2NDIsImlhdCI6MTYxOTMzNjA0MiwidXNlcm5hbWUiOiJUYXJ0ZSJ9.6P-BwyEo1v5cCdFw1JTnsX5W9qw0XpQqMoy6SkhAFgU"
	r.Header.Set("Authorization", fmt.Sprintf("Bearer %v", token))
	w := httptest.NewRecorder()

	// We change this line for test the real routing system.
	srv.serveHTTP(w, r)
	assert.Equal(t, http.StatusOK, w.Code)
}
