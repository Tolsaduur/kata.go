# Mise en place

Faire la commandes pour le créer le .env :
    make .env db_user=*** db_pass=*** db=*** base_ip_network=*.*.*

Faire la commande pour lancer les services postgreSQL, build le projet Golang et le lancer :
    make up


Piste d'amélioration

Axe du traitement des requêtes HTTP
    Ajouter de la pagination dans le renvoie des films
    Ajouter la possibilité de rechercher des films suivant des critéres (rendre dynamyique via des GET)

Compléxifier la base de donnée (genres, acteurs, réalisateurs, notes,  etc ...)

Ajouter une liste de souhaits pour l'associer à un user

Tester le maximum de composant pour commencer à "sentir" les éléments important à tester.

Mettre en place de la CI