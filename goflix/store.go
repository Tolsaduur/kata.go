package main

import (
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var schema = `
CREATE TABLE IF NOT EXISTS movie
(
	id SERIAL PRIMARY KEY,
	title TEXT,
	release_date DATE,
	duration INT,
	trailer_url TEXT
);

CREATE TABLE IF NOT EXISTS users
(
	id SERIAL PRIMARY KEY,
	username TEXT,
	password TEXT
);
`

type Store interface {
	Open() error
	Close() error

	GetMovies() ([]*Movie, error)
	GetMovieById(id int64) (*Movie, error)
	CreateMovie(m *Movie) error
	CheckUser(username string, password string) bool
}
type dbStore struct {
	db *sqlx.DB
}

func (store *dbStore) Open() error {
	db, err := sqlx.Connect("postgres", "host=0.0.0.0 port=5432 user=admin password=admin dbname=goflix sslmode=disable")
	if err != nil {
		return err
	}
	log.Println("Connected to DB")
	db.MustExec(schema)
	store.db = db

	return nil
}

func (store *dbStore) Close() error {
	return store.db.Close()
}

func (store *dbStore) GetMovies() ([]*Movie, error) {
	var movies []*Movie
	err := store.db.Select(&movies, "Select * From movie")
	if err != nil {
		return movies, err
	}
	return movies, nil
}

func (store *dbStore) GetMovieById(id int64) (*Movie, error) {
	var movie = &Movie{}
	err := store.db.Get(movie, "Select * From movie Where id = $1", id)
	if err != nil {
		return movie, err
	}

	return movie, nil
}

func (store *dbStore) CreateMovie(m *Movie) error {
	var id int64 = 0
	sqlStatement := `
		INSERT INTO movie (title, release_date, duration, trailer_url) 
		VALUES ($1, $2, $3, $4) 
		RETURNING id
	`
	err := store.db.QueryRow(sqlStatement, m.Title, m.ReleaseDate, m.Duration, m.TrailerURL).Scan(&id)
	if err != nil {
		return err
	}

	m.ID = id
	return err
}

func (store *dbStore) CheckUser(username string, password string) bool {
	var user = &User{}
	err := store.db.Get(user, "Select id FROM users WHERE username = $1 AND password = $2", username, password)

	return nil == err
}
