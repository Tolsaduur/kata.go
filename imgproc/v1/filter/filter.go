package filter

import "github.com/disintegration/imaging"

// Filter interface with method Process
type Filter interface {
	Process(srcPath, dstPath string) error
}

// GreyScale struct
type GreyScale struct{}

// Process for apply a greyscale filter on a image.
func (g GreyScale) Process(srcPath, dstPath string) error {

	src, err := imaging.Open(srcPath)
	if err != nil {
		return err
	}

	img := imaging.Grayscale(src)

	err = imaging.Save(img, dstPath)
	if err != nil {
		return err
	}

	return nil
}

// Blur struct
type Blur struct{}

// Process for apply a blur filter on a image.
func (b Blur) Process(srcPath, dstPath string) error {

	src, err := imaging.Open(srcPath)
	if err != nil {
		return err
	}

	img := imaging.Blur(src, 3.5)

	err = imaging.Save(img, dstPath)
	if err != nil {
		return err
	}

	return nil
}
