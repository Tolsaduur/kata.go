package main

import (
	"flag"
	"fmt"
	"os"
	"time"

	"kata.go/imgproc/v1/filter"
	"kata.go/imgproc/v1/task"
)

func main() {
	srcDir := flag.String("src", "", "Input directory")
	dstDir := flag.String("dst", "", "Outout directory")
	filterType := flag.String("filter", "greyscale", "grayscale/blur")
	processType := flag.String("process", "waitgroup", "waitgroup/channel")
	poolSize := flag.Int("poolsize", 4, "Pool size for channel process")

	flag.Parse()

	var f filter.Filter
	switch *filterType {
	case "grayscale":
		f = filter.GreyScale{}
	case "blur":
		f = filter.Blur{}
	default:
		fmt.Println("Unknow filter type : accept only grayscale or blur")
		os.Exit(1)
	}

	var t task.Tasker
	switch *processType {
	case "waitgroup":
		t = task.NewWaitGrpTask(*srcDir, *dstDir, f)
	case "channel":
		t = task.NewChanTask(*srcDir, *dstDir, f, *poolSize)
	default:
		fmt.Println("Unknow process type : accept only waitgroup or channel")
		os.Exit(1)
	}

	start := time.Now()
	t.Process()
	elapsed := time.Since(start)
	fmt.Printf("Image Processing took %s\n", elapsed)
}
