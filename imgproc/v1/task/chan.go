package task

import (
	"fmt"
	"path"
	"path/filepath"

	"kata.go/imgproc/v1/filter"
)

type ChanTask struct {
	dirCtx
	Filter   filter.Filter
	PoolSize int
}

func NewChanTask(srcDir, dstDir string, filter filter.Filter, poolSize int) Tasker {
	return &ChanTask{
		dirCtx: dirCtx{
			SrcDir: srcDir,
			DstDir: dstDir,
			files:  buildFileList(srcDir),
		},
		Filter:   filter,
		PoolSize: poolSize,
	}
}

type jobReq struct {
	src string
	dst string
}

func (c *ChanTask) Process() error {
	size := len(c.files)
	jobs := make(chan jobReq, size)
	results := make(chan string, size)

	// init worker
	for i := 1; i <= c.PoolSize; i++ {
		go worker(i, c, jobs, results)
	}

	// start jbos
	for _, f := range c.files {
		filename := filepath.Base(f)
		dst := path.Join(c.DstDir, filename)
		jobs <- jobReq{
			src: f,
			dst: dst,
		}
	}
	close(jobs)

	// Display result
	for r := range results {
		fmt.Printf("Job finished for %v\n", r)
	}

	return nil
}

func worker(id int, c *ChanTask, jobs <-chan jobReq, results chan<- string) {
	for j := range jobs {
		fmt.Printf("worker %d started job %v\n", id, j.dst)
		c.Filter.Process(j.src, j.dst)
		results <- j.dst
	}
	close(results)
}
