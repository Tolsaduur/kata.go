# Kata : Hangman

Le but de ce kata est de reproduire un jeu du pendu !

# Régle du jeu

Un mot sera choisie au hasard parmis un dictionnaire de mots.;

Le joueur devra deviner le mots en proposant des lettres :
    - Quand une lettre est présente dans le mot, on remplace les vide par la lettre
    - Quand une lettre n'est pas présente, on rajoute un élement du pendu.

Le joueur à 8 tentative pour trouvé le mot.

## Features 

    - Affichage & dessin du pendu en console.
    - Saisie utilisateur faite à partir de la lecture de la saisie clavier.
    - Chargement aléatoire du à partir d'un fichier de dictionnaire.
    - Affichage de l'état de la partie :
        - Le pendu
        - Lettres trouvées
        - Lettres proposées
## Bonus
    - Ajouter un systéme d'indice pour trouver le mot
    - Ajouter un systéme de score suivant le nombre de lettre utilisée et les indices utilisée

# Kata : Dictionary

Le but est mettre en place d'une application qui permet de stocker des mots avec une définition dans une base de donnée persistance.

## Features

    - Pouvoir ajouter un mot + définition
    - Voir la définition d'un mot
    - Lister les mots + définition
    - Supprimer un mot + définition

## Bonus
    - Ajouter un tag par définition / Lister par tag
        - #tech, #photo, ...
    - Trier par date / tag
    - Afficher des statistiques
        - Lettres les plus représentées
        - Longueur moyenne des mots / définitions


# Kata : Generate Certificats

Le but est d'écrire un programme qui génére des certificats d'accomplissement au format PDF ou HTML.

## Features 
    - Input : un fichier CSV avec par ligne :
        - Le cours liée au certificat
        - le prénom / nom de la personne
        - la date d'obtention du certificat
    - Output : le certificat au format souhaité : PDF ou HTML

## Bonus
    - Ajouter une autre format de fichier d'entrée (JSON, XML, etc ...)
        - Utiliser des interfaces pour la parsing ?
    - Améliorer le rendu du HTML en ajoutant du CSS et des images.
    - Packager le résultat HTML dans un fichier ZIP avec le CSS et les images.
        - package archive/zip
    - Supporter un mode portrait pour le PDF

# Kata : Image Processing

Le but est d'appliquer des flitres sur des dossiers images. On fait ainsi travailler l'ensemble des processeurs pour faire les traitement.

## Features 
    - Mise en place de deux filtres :
        - Noir et blanc
        - Flou gaussien
    - Avoir la possibilité de définir la maniére de traitement des fichiers.
        - Channels Pools, qu'on pourra définir
        - WaitGroup
    - Le tout doit être fonctionnel en CLI.

## Bonus
    - Ajout d'autres filtres issus de Imaginc
        - Recoder les filtres
    - Proposer un systéme de redimensionnement d'images
    - Enregistrer dans un autres format
    - Rendre les filtres chainable
        - ex : grayscale => blur => ?
        - Manipuler un objet images qu'on baladerait de filtre en filtre