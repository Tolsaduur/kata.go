package cert

import (
	"testing"
	"time"
)

func TestValidCertData(t *testing.T) {
	c, err := New("Golang", "Bob", "2018-05-13")
	if err != nil {
		t.Errorf("Cert data should be valid. err=%v", err)
	}
	if c == nil {
		t.Errorf("Cert should be a valid reference. got=nil")
	}

	if c.Course != "GOLANG COURSE" {
		t.Errorf("Course name is not valid. expected='GOLANG COURSE' got='%v'", c.Course)
	}

	if c.Name != "BOB" {
		t.Errorf("Name is not valid. expected='Bob' got='%v'", c.Name)
	}

	g, _ := time.Parse("02-01-2006", "13-05-2018")
	if c.Date != g {
		t.Errorf("Date is not valid. expected='13-05-2018' got='%v'", c.Date)
	}

	if c.LabelDate != "Date: 13/05/2018" {
		t.Errorf("LabelDate is not valid. expected='Date: 13/05/2018' got='%v'", c.Date)
	}
}

func TestCourseEmptyValue(t *testing.T) {
	_, err := New("", "Bob", "2018-11-11")
	if err == nil {
		t.Errorf("Error should be returned on an empty course")
	}
}

func TestCourseTooLong(t *testing.T) {
	course := "jfjiozjfiojrzgjiorejgioejriogjeriogjeorijlskdkfdsklfjsdklfjziogjiozfjiozejfekljsfklsjfklsdjfklsjiofjsioef"
	_, err := New(course, "Bob", "2018-11-11")
	if err == nil {
		t.Errorf("Error should be returned on a too long course name (course=%s)", course)
	}
}

func TestNameEmptyValue(t *testing.T) {
	_, err := New("Golang Course", "", "2018-11-11")
	if err == nil {
		t.Errorf("Error should be returned on an empty name.")
	}
}

func TestNameTooLong(t *testing.T) {
	name := "Lorem DJDJZdjiediejfijzefzeiofjzeiofjzeoifjzeoiofjzepipfjzeoiogjzpofzeopfkopzekfeopkfgzpokdqsdqpdzp"
	_, err := New("Golang Course", name, "2018-11-11")
	if err == nil {
		t.Errorf("Error should be returned on a too long name (name=%s)", name)
	}
}
