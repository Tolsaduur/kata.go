package main

import (
	"flag"
	"fmt"
	"os"

	"kata.go/certificat/v1/cert"
	"kata.go/certificat/v1/html"
	"kata.go/certificat/v1/pdf"
)

func main() {
	outputType := flag.String("type", "pdf", "Output type of the certificate")
	fileToParse := flag.String("file", "", "CSV file to parse for generate certificate.")
	flag.Parse()

	if len(*fileToParse) == 0 {
		fmt.Println("Invalid file : empty")
		os.Exit(1)
	}

	var saver cert.Saver
	var err error
	switch *outputType {
	case "html":
		saver, err = html.New("output")
	case "pdf":
		saver, err = pdf.New("output")
	default:
		fmt.Printf("Unknow output type. got=%v\n", *outputType)
	}

	if err != nil {
		fmt.Printf("Could not create generator: %v\n", err)
		os.Exit(1)
	}

	certs, err := cert.ParseCSV(*fileToParse)
	if err != nil {
		fmt.Printf("Error to parse file %s. error=%v\n", *fileToParse, err)
		os.Exit(1)
	}

	for _, c := range certs {
		err = saver.Save(*c)
		if err != nil {
			fmt.Printf("Could not save certificate : %v\n", err)
		}
	}
}
