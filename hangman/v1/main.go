package main

import (
	"fmt"
	"os"

	"kata.go/hangman/v1/dictionary"
	"kata.go/hangman/v1/hangman"
)

/*
	Proposition d'architecture

		- Forte sépération de la logique et de l'I/O
			- Input Clavier.
			- Output écran console.
		- Chargement du dictionnaire isolé
*/

func main() {
	err := dictionary.Load("words.txt")
	if err != nil {
		fmt.Printf("Erreur de lecture du fichier : %v", err)
		os.Exit(1)
	}

	g, err := hangman.New(8, dictionary.PickWord(), 4)
	if err != nil {
		fmt.Printf("Error for make new game : %v", err)
		os.Exit(1)
	}

	hangman.DrawWelcome()

	guess := ""
	for {
		hangman.Draw(g, guess)

		switch g.State {
		case "won", "lost":
			os.Exit(0)
		}

		l, err := hangman.ReadGuess()
		if err != nil {
			fmt.Printf("Erreur de lecture du termial : %v", err)
			os.Exit(1)
		}
		g.MakeAGuess(l)
	}
}
