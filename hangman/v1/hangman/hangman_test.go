package hangman

import (
	"strings"
	"testing"
)

func TestGetTips(t *testing.T) {
	var g, _ = New(4, "bureau", 3)

	tips := g.GetTips()

	goodTips := false
	for _, l := range g.Letters {
		if l == tips {
			goodTips = true
		}
	}

	if !goodTips {
		t.Errorf("Erreur sur l'indice, le mot %s ne contient pas la lettre %s, got=%v", g.Letters, tips, goodTips)
	}

	if g.TipsNumber != 2 {
		t.Errorf("Le nombre d'indice n'a pas été décrémenté.")
	}
}

func TestLetterInWord(t *testing.T) {
	word := []string{"g", "o", "l", "a", "n", "g"}
	guess := "g"
	hasLetter := letterInWord(guess, word)
	if !hasLetter {
		t.Errorf("Le mot %s contient la lettre %s, got=%v", word, guess, hasLetter)
	}
}

func TestLetterNotInWord(t *testing.T) {
	word := []string{"g", "o", "l", "a", "n", "g"}
	guess := "w"
	hasLetter := letterInWord(guess, word)
	if hasLetter {
		t.Errorf("Le mot %s ne contient pas la lettre %s, got=%v", word, guess, hasLetter)
	}
}

func TestInvalidWord(t *testing.T) {
	_, err := New(3, "", 3)
	if err == nil {
		t.Errorf("Error should be retrun when using a invalid word=''")
	}
}

func TestGameGoodGuess(t *testing.T) {
	g, _ := New(8, "bureau", 3)
	guess := "b"
	g.MakeAGuess(guess)

	validState(t, "goodguess", g.State)

	if len(g.UsedLetters) == 0 || g.UsedLetters[0] != strings.ToUpper(guess) {
		t.Errorf("'%v' must be in g.UsedLetters slice.", guess)
	}

	IsInFoundLetters := false

	for _, fl := range g.FoundLetters {
		if fl == strings.ToUpper(guess) {

			IsInFoundLetters = true
			break
		}
	}

	if !IsInFoundLetters {
		t.Errorf("'%v' must be in g.FoundLetters slice.", guess)
	}
}

func TestAlreadyGuess(t *testing.T) {
	g, _ := New(8, "bureau", 3)
	guess := "b"
	g.MakeAGuess(guess)
	g.MakeAGuess(guess)

	validState(t, "alreadyguess", g.State)
}

func TestBadGuess(t *testing.T) {
	g, _ := New(8, "bureau", 3)
	guess := "m"
	g.MakeAGuess(guess)

	validState(t, "badguess", g.State)
}

func TestGameWon(t *testing.T) {
	g, _ := New(8, "bureau", 3)
	g.MakeAGuess("b")
	g.MakeAGuess("u")
	g.MakeAGuess("r")
	g.MakeAGuess("e")
	g.MakeAGuess("a")
	g.MakeAGuess("u")

	validState(t, "won", g.State)
}

func TestGameLost(t *testing.T) {
	g, _ := New(3, "bureau", 3)
	g.MakeAGuess("w")
	g.MakeAGuess("t")
	g.MakeAGuess("m")

	validState(t, "lost", g.State)
}

func validState(t *testing.T, expectedState, actualState string) bool {
	if expectedState != actualState {
		t.Errorf("Game state should be %v. got=%v", expectedState, actualState)
		return false
	}
	return true
}
