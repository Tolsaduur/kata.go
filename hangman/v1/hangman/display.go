package hangman

import "fmt"

func DrawWelcome() {
	fmt.Println(`
	 ___                  _        ______              _         _  
	|_  |                | |       | ___ \            | |       | | 
	  | | ___ _   _    __| |_   _  | |_/ /__ _ __   __| |_   _  | | 
	  | |/ _ \ | | |  / _  | | | | |  __/ _ \ '_ \ / _' | | | | | | 
  /\__/ /  __/ |_| | | (_| | |_| | | | |  __/ | | | (_| | |_| | |_| 
  \____/ \___|\__,_|  \__,_|\__,_| \_|  \___|_| |_|\__,_|\__,_| (_) 																
	`)
}

func Draw(g *Game, guess string) {
	drawTurns(g.TurnsLeft)
	drawState(g, guess)

	if g.State != WonState && g.State != LostState {
		drawTurnsLeft(g.NumbersTry, g.TurnsLeft)
	}
}

func drawTurns(l int) {
	var draw string
	switch l {
	case 0:
		draw = `
		____
	   |    |
	   |    o
	   |   /|\	
	   |    |
	   |   / \
	 _ | _	
	|	  |____
	|	  	   |
	|__________|
		`
	case 1:
		draw = `
		____
	   |    |
	   |    o
	   |   /|\	
	   |    |
	   |   
	 _ | _	
	|	  |____
	|	  	   |
	|__________|
		`
	case 2:
		draw = `
		____
	   |    |
	   |    o
	   |    |	
	   |    |
	   |   
	 _ | _	
	|	  |____
	|	  	   |
	|__________|
		`
	case 3:
		draw = `
	    ____
	   |    |
	   |    o
	   |    
	   |    
	   |   
	 _ | _	
	|	  |____
	|	  	   |
	|__________|
		`
	case 4:
		draw = `
	    ____
	   |    |
	   |    
	   |    
	   |    
	   |   
	 _ | _	
	|	  |____
	|	  	   |
	|__________|
		`
	case 5:
		draw = `
	    ____
	   |    
	   |    
	   |    
	   |    
	   |   
	 _ | _	
	|	  |____
	|	  	   |
	|__________|
		`
	case 6:
		draw = `
	    
	   |    
	   |    
	   |    
	   |    
	   |   
	 __|__	
	|	  |____
	|	  	   |
	|__________|
		`
	case 7:
		draw = `
	 _____	
	|	  |____
	|	  	   |
	|__________|
		`
	case 8:
		draw = `
		
		`
	}

	fmt.Println(draw)
}

func drawState(g *Game, guess string) {
	fmt.Print("Guessed: ")
	drawLetters(g.FoundLetters)

	fmt.Print("Used: ")
	drawLetters(g.UsedLetters)

	switch g.State {
	case GoodguessState:
		fmt.Println("Good guess !")
	case AlreadyguessState:
		fmt.Printf("Letter '%s' was already used\n", guess)
	case BadguessState:
		fmt.Printf("Bad guess '%s' is not in the word\n", guess)
	case LostState:
		fmt.Print("You lost :( ! The word was : ")
		drawLetters(g.Letters)
	case WonState:
		fmt.Print("You won :) ! The word was : ")
		drawLetters(g.Letters)
	case TipsState:
		drawTips(g)
	}
}

func drawLetters(l []string) {
	for _, c := range l {
		fmt.Printf("%v ", c)
	}
	fmt.Println()
}

func drawTurnsLeft(numbersTry, turnsLeft int) {
	fmt.Printf("It's your %vth try. It's left %v try before loose !\n", numbersTry, turnsLeft)
}

func drawTips(g *Game) {
	if g.TipsNumber == 0 {
		fmt.Println("Vous avez utilisez tous vos indice !")
	} else {
		tips := g.GetTips()
		fmt.Printf("Essayez cette lettre : %s\n", tips)
		fmt.Printf("Attention il  ne vous reste que %d indices\n", g.TipsNumber)
	}
}
