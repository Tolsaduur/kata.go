package hangman

import (
	"fmt"
	"math/rand"
	"strings"
	"time"
)

const GoodguessState string = "goodguess"
const AlreadyguessState string = "alreadyguess"
const BadguessState string = "badguess"
const LostState string = "lost"
const WonState string = "won"
const TipsState string = "tips"
const TipsChar = "?"

type Game struct {
	State        string   // Game state
	Letters      []string // Letters in the word to find
	FoundLetters []string // Good guesses
	UsedLetters  []string // Used letters
	TurnsLeft    int      // Remaining attemps
	NumbersTry   int      // Number of try
	TipsNumber   int
}

func New(turns int, word string, tipsNumber int) (*Game, error) {
	if len(word) < 3 {
		return nil, fmt.Errorf("Word '%s' must be at least 3 characters. got=%v", word, len(word))
	}

	letters := strings.Split(strings.ToUpper(word), "")
	found := make([]string, len(letters))
	for i := 0; i < len(letters); i++ {
		found[i] = "_"
	}

	g := &Game{
		State:        "",
		Letters:      letters,
		FoundLetters: found,
		UsedLetters:  []string{},
		TurnsLeft:    turns,
		NumbersTry:   0,
		TipsNumber:   tipsNumber,
	}

	return g, nil
}

func (g *Game) MakeAGuess(guess string) {
	guess = strings.ToUpper(guess)

	switch g.State {
	case "won", "lost":
		return
	}

	if guess == TipsChar {
		g.State = TipsState
		return
	}

	if letterInWord(guess, g.UsedLetters) {
		g.State = AlreadyguessState
		g.NumbersTry++
	} else if letterInWord(guess, g.Letters) {
		g.State = GoodguessState
		g.RevealLetter(guess)
		g.NumbersTry++

		if hasWon(g.Letters, g.FoundLetters) {
			g.State = WonState
		}
	} else {
		g.State = BadguessState
		g.UsedLetters = append(g.UsedLetters, guess)
		g.TurnsLeft--
		g.NumbersTry++

		if g.TurnsLeft == 0 {
			g.State = LostState
		}
	}
}

func letterInWord(guess string, letters []string) bool {
	for _, l := range letters {
		if l == guess {
			return true
		}
	}

	return false
}

func (g *Game) RevealLetter(guess string) {
	g.UsedLetters = append(g.UsedLetters, guess)
	for i, l := range g.Letters {
		if l == guess {
			g.FoundLetters[i] = guess
		}
	}
}

func hasWon(letters []string, foundLetters []string) bool {
	for i := range letters {
		if letters[i] != foundLetters[i] {
			return false
		}
	}

	return true
}

func (g *Game) GetTips() string {
	lettersNotFound := []string{}

	for _, l := range g.Letters {
		isUsed := false
		for _, ul := range g.UsedLetters {
			if ul == l {
				isUsed = true
				break
			}
		}

		if !isUsed {
			lettersNotFound = append(lettersNotFound, l)
		}
	}

	g.TipsNumber--

	rand.Seed(time.Now().UnixNano())
	return lettersNotFound[rand.Intn(len(lettersNotFound))]
}
