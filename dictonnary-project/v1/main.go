package main

import (
	"flag"
	"fmt"
	"os"

	"kata.go/dictonnary-project/v1/dictionnary"
)

func main() {
	action := flag.String("action", "list", "Action to perform on dictionary")

	d, err := dictionnary.New("./badger")
	HandleErr(err)
	defer d.Close()

	flag.Parse()
	switch *action {
	case "list":
		actionList(d)
	case "add":
		actionAdd(d, flag.Args())
	case "define":
		actionDefine(d, flag.Args())
	case "delete":
		actionDelete(d, flag.Args())
	default:
		fmt.Printf("Unknow action: %v\n", action)
	}
}

func actionList(d *dictionnary.Dictionary) {
	words, entries, err := d.List()
	HandleErr(err)
	fmt.Println("Dictionary content")
	for _, word := range words {
		fmt.Println(entries[word])
	}
}

func actionAdd(d *dictionnary.Dictionary, args []string) {
	word := args[0]
	defintion := args[1]

	err := d.Add(word, defintion)
	HandleErr(err)
	fmt.Printf("'%v' adding to dictionary\n", word)
}

func actionDelete(d *dictionnary.Dictionary, args []string) {
	word := args[0]

	err := d.Remove(word)
	HandleErr(err)
	fmt.Printf("'%v' deleting to dictionary\n", word)
}

func actionDefine(d *dictionnary.Dictionary, args []string) {
	word := args[0]

	entry, err := d.Get(word)
	HandleErr(err)

	fmt.Println(entry)
}

func HandleErr(err error) {
	if err != nil {
		fmt.Printf("Dictionary error: %v\n", err)
		os.Exit(1)
	}
}
